/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct{
	uint16_t brakeAddress;
	uint16_t brakeLedNum;
	uint16_t rightIndicatorAddress;
	uint16_t rightIndicatorLeds;
	uint16_t leftIndicatorAddress;
	uint16_t leftIndicatorLeds;
}ledAddress_t;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
#define		LED_LOW		0x08
#define		LED_HIGH	0x0c
#define 	LED_NUM		8

#define 	LED_ADDRESS(_ledAddress) (_ledAddress*12)

#define 	LED_LEFT_INDICATE	1
#define 	LED_RIGHT_INDICATE	2

enum colours{
	GREEN, RED, BLUE
};

enum ledStates{
	LED_OFF, LED_IDLE, LED_BRAKES, LED_HAZARD, LED_LEFT_INDICATOR, LED_RIGHT_INDICATOR
};
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;
DMA_HandleTypeDef hdma_spi1_tx;

/* USER CODE BEGIN PV */
uint8_t ledBuff[LED_NUM*12];

uint8_t ledState = LED_OFF;

ledAddress_t ledAddresses = {
		LED_ADDRESS(3),
		1,
		LED_ADDRESS(2),
		3,
		LED_ADDRESS(4),
		3
};

volatile uint16_t globalLedAddressCounter = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_SPI1_Init(void);
/* USER CODE BEGIN PFP */
void init_led();
void led_off();
void led_brake();
void led_hazard();
void led_indicate(uint8_t);

void led_update_colour(uint16_t, uint8_t, uint8_t, uint8_t);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void init_led()
{
	uint8_t _low = LED_LOW;
	_low <<= 4;
	_low |= LED_LOW;
	for(uint8_t i=0; i<sizeof(ledBuff); ++i){
		ledBuff[i] = _low;
	}

	HAL_Delay(1);
	HAL_SPI_Transmit_DMA(&hspi1, ledBuff, sizeof(ledBuff));
}

void led_off()
{
	init_led();
}

void led_brake()
{
	uint8_t low=LED_LOW, high=LED_HIGH;
	low <<= 4;
	high <<= 4;
	low |= LED_LOW;
	high |= LED_HIGH;

	uint8_t currColour = GREEN;
	uint8_t bitIndex = 0;
	uint16_t startAddress = ledAddresses.brakeAddress;

	for(uint16_t i=0; i<(ledAddresses.brakeLedNum * 12); ++i){
		switch(currColour){
		case GREEN:{
			bitIndex += 2;
			ledBuff[startAddress++] = low;
			if(bitIndex >= 8){
				bitIndex = 0;
				currColour = RED;
			}
			break;
		}
		case RED:{
			bitIndex += 2;
			ledBuff[startAddress++] = high;
			if(bitIndex >= 8){
				bitIndex = 0;
				currColour = BLUE;
			}
			break;
		}
		case BLUE:{
			bitIndex += 2;
			ledBuff[startAddress++] = low;
			if(bitIndex >= 8){
				bitIndex = 0;
				currColour = GREEN;
			}
			break;
		}
		default:{
			break;
		}
		}
	}

	HAL_Delay(1);
	HAL_SPI_Transmit_DMA(&hspi1, ledBuff, sizeof(ledBuff));
}

void led_hazard()
{
	if(globalLedAddressCounter >= ledAddresses.rightIndicatorLeds || globalLedAddressCounter >= ledAddresses.leftIndicatorLeds || globalLedAddressCounter == 0){
		globalLedAddressCounter = 0;
		HAL_Delay(150);
		led_off();
		HAL_Delay(400);
	}

	//Fill Right
	led_update_colour((ledAddresses.rightIndicatorAddress - LED_ADDRESS(globalLedAddressCounter)), 126, 255, 0);
	//Fill Left
	led_update_colour((ledAddresses.leftIndicatorAddress + LED_ADDRESS(globalLedAddressCounter)), 126, 255, 0);

	globalLedAddressCounter++;

	HAL_Delay(1);
	HAL_SPI_Transmit_DMA(&hspi1, ledBuff, sizeof(ledBuff));
}

void led_indicate(uint8_t direction)
{
	if(globalLedAddressCounter >= ledAddresses.rightIndicatorLeds || globalLedAddressCounter >= ledAddresses.leftIndicatorLeds || globalLedAddressCounter == 0){
		globalLedAddressCounter = 0;
		HAL_Delay(150);
		led_off();
		HAL_Delay(400);
	}

	if(direction == LED_RIGHT_INDICATOR){
		led_update_colour(ledAddresses.rightIndicatorAddress - LED_ADDRESS(globalLedAddressCounter), 126, 255, 0);
	}
	else if(direction == LED_LEFT_INDICATOR){
		led_update_colour(ledAddresses.leftIndicatorAddress + LED_ADDRESS(globalLedAddressCounter), 126, 255, 0);
	}

	globalLedAddressCounter++;

	HAL_Delay(1);
	HAL_SPI_Transmit_DMA(&hspi1, ledBuff, sizeof(ledBuff));
}

void led_update_colour(uint16_t _address, uint8_t _g, uint8_t _r, uint8_t _b)
{
	uint8_t msNibble = 1;
	uint8_t spiByte = 0;

	uint32_t grbColour = _g;
	grbColour <<= 8;
	grbColour |= _r;
	grbColour <<= 8;
	grbColour |= _b;

	for(uint8_t i=0; i<24; ++i){
		if((grbColour << i) & 0x00800000){
			spiByte |= LED_HIGH;
		}
		else{
			spiByte |= LED_LOW;
		}

		if(msNibble){
			msNibble = 0;
			spiByte <<= 4;
		}
		else{
			msNibble = 1;
			ledBuff[_address++] = spiByte;
			spiByte = 0x00;
		}
	}
}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_SPI1_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  init_led();
  while (1)
  {
	  //led_brake();
	  //HAL_Delay(2000);
	  //init_led();
	  led_hazard();
	  HAL_Delay(70);
	  //led_indicate(LED_LEFT_INDICATOR);
	  //led_brake();
	  //HAL_Delay(200);
	  //led_off();
	  //HAL_Delay(200);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel3_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
